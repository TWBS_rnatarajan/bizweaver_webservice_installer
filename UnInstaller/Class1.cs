﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.Administration;
using Microsoft.Win32;

namespace TWBS.UnInstaller
{
    public class UnInstaller
    {
        ServerManager iisManager;
        public void UnInstallService(int Type)
        {
            try
            {
                UninstallWebsite();

            }
            catch (Exception Ex)
            {


                throw (Ex);
            }
        }
        public void UninstallWebsite()
        {
            try
            {


                string WebSiteName = "Versago";// "BWPortal    ";
                iisManager = new ServerManager();
                Site site = iisManager.Sites[WebSiteName];
                if (site != null)
                {
                    string ApplicationPath = "/BWService";
                    Application app = site.Applications[ApplicationPath]; 
                    if (app == null)
                        return;
                    Boolean uninstallWebsite = true;
                    foreach (Application eApp in site.Applications)
                    {
                        if (eApp.Path != "BWService" && eApp.Path!="aspnet_client") { uninstallWebsite = false; break; }
                    }
                    if (uninstallWebsite)
                    { iisManager.Sites.Remove(site); }
                    else { site.Applications.Remove(app); }

                    iisManager.CommitChanges();
                    DeleteRegistryKey("BWService", true);

                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

                throw(ex);
            }

        }

        private void DeleteRegistryKey(string Key, bool DeleteRoot)//Client,Admin,Service
        {
            try
            {
                //HKEY_LOCAL_MACHINE
                if (DeleteRoot == false)
                {
                    string RegistryPath = "";
                    if (Environment.Is64BitOperatingSystem)
                    {
                        RegistryPath = @"SOFTWARE\Wow6432Node\TWBS\BWPortal";
                    }
                    else
                    {
                        RegistryPath = @"SOFTWARE\TWBS\BWPortal";
                    }
                    RegistryKey ClientKey = Registry.LocalMachine.OpenSubKey(RegistryPath, true);
                    if (ClientKey != null)
                    {
                        if (ClientKey.OpenSubKey(Key) != null)
                        {
                            ClientKey.DeleteSubKey(Key);
                        }
                    }
                }
                else
                {
                    string RegistryPath = "";
                    if (Environment.Is64BitOperatingSystem)
                    {
                        RegistryPath = @"SOFTWARE\Wow6432Node\TWBS\BWPortal";
                    }
                    else
                    {
                        RegistryPath = @"SOFTWARE";
                    }
                    RegistryKey ClientKey = Registry.LocalMachine.OpenSubKey(RegistryPath, true);
                    if (ClientKey != null)
                    {
                        if (ClientKey.OpenSubKey("TWBS\\BWPortal") != null)
                        {
                            ClientKey.DeleteSubKey("TWBS\\BWPortal");
                            if (ClientKey.OpenSubKey("TWBS").SubKeyCount == 0)
                            {
                                ClientKey.DeleteSubKey("TWBS");
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

       
    }
}
