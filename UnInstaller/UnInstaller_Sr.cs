﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Collections.Specialized;


namespace TWBS.UnInstaller
{
    [RunInstaller(true)]
    public partial class UnInstaller_Sr : System.Configuration.Install.Installer
    {
        UnInstaller un;
        public UnInstaller_Sr()
        {
            try
            {
                InitializeComponent();
                //System.Windows.Forms.MessageBox.Show("Start");
                //un = new UnInstaller();
                //if (this.Context != null)
                //{
                //    StringDictionary paramStringDictionary = this.Context.Parameters;
                //    if (paramStringDictionary != null && paramStringDictionary.Count > 0)
                //    {
                //        string Type = paramStringDictionary["apptype"];
                //        un.UnInstallService(Type);

                //        UService();
                //        System.Windows.Forms.MessageBox.Show(Type);


                //    }
                //    else
                //    {
                //        System.Windows.Forms.MessageBox.Show("No Param");
                //        //un.UnInstallService("No Parameter");
                //    }
                //}
                //else
                //{
                //    System.Windows.Forms.MessageBox.Show("this.Context = nul");
                //}
            }
            catch (Exception Ex)
            {

                System.Windows.Forms.MessageBox.Show(Ex.Message);
            }
            
        }

        private void UService(int Type)
        {
            try
            {
                un.UnInstallService(Type);
            }
            catch (Exception Ex)
            {
                
                throw(Ex);
            }
        }

        public override void Uninstall(IDictionary savedState)
        {
            try
            {
                base.Uninstall(savedState);
              //  System.Windows.Forms.MessageBox.Show("Start");
                un = new UnInstaller();
                if (this.Context != null)
                {
                    un.UnInstallService(0);
                }
                else
                {
                    //System.Windows.Forms.MessageBox.Show("this.Context = nul");
                }
            }
            catch (Exception Ex)
            {

                System.Windows.Forms.MessageBox.Show(Ex.Message);
            }
            
            
        }
    }
}
