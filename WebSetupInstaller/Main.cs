﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Microsoft.Win32;
using Microsoft.Web.Administration;
using System.Security.Cryptography.X509Certificates;
namespace WebSetupInstaller
{
    public class InstallerResult
    {
        public Boolean pSuccess { get; set; }
        public string pMessage { get; set; }
    }
    public class Main
    {
        public string PathParam, strTempFolder = "";
        Version IISVersion;
        Boolean EnableSSLFeature = false;
        byte[] SSLCertificateHash =null;
        int IISPort = 7071;
        WebInstaller objWebInstaller;
        private bool IsServiceInstalled = false, SkipPortChecking = false;
        private Boolean ShowForm()
        {
            try
            {
                if (IISVersion.Major >= 7)
                {

                    objWebInstaller = new WebInstaller();
                    objWebInstaller.physicalpath = PathParam;
                    objWebInstaller.Port = Convert.ToString(IISPort);
                    objWebInstaller.IsEnableSSL = EnableSSLFeature;
                    objWebInstaller.SSLCertificateHash = SSLCertificateHash;
                    objWebInstaller.IsService = true;
                    CopyTempFiles();
                    return InstallerAction();

                }
                else
                {
                    throw new Exception("BWService installer requires IIS version 7 or later");
                }
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

        public static Dictionary<byte[], string> getSSLCertificateList()
        {
            Dictionary<byte[], string> SSLCerti = new Dictionary<byte[], string>();
            var store = new X509Store(StoreLocation.LocalMachine);
            store.Open(OpenFlags.ReadOnly);
            var certificates = store.Certificates;
            foreach (var certificate in certificates)
            {
                SSLCerti.Add(certificate.GetCertHash(), certificate.FriendlyName);
                //Console.WriteLine(certificate.FriendlyName);
            }
            store.Close();
            return SSLCerti;
        }

        public InstallerResult InstallBWPortal(string strDestinationPath,int port,Boolean EnableSSL,byte[] SSLCertificateHashCode)
        {
            InstallerResult IResut = new InstallerResult();
            
            try
            {
                if (strDestinationPath.LastIndexOf('\\') == strDestinationPath.Length - 1)
                {
                    strDestinationPath = strDestinationPath.Remove(strDestinationPath.Length - 1);
                }

                PathParam = strDestinationPath + @"\BWPortal";
                if (System.IO.Directory.Exists(PathParam))
                    System.IO.Directory.CreateDirectory(PathParam);

                if(port>0)
                    IISPort = port;
                EnableSSLFeature = EnableSSL;
                SSLCertificateHash = SSLCertificateHashCode;

                strTempFolder = strDestinationPath + @"\BWPortal";
                if (System.IO.Directory.Exists(strTempFolder) == false)
                    System.IO.Directory.CreateDirectory(strTempFolder);

                GetRegistryValue();
                GetIisVersion();
                if (ShowForm())
                { 

                    IResut.pSuccess = true;
                    IResut.pMessage = "";
                    return IResut;
                }
                else
                {
                    IResut.pSuccess = false;
                    IResut.pMessage = "Unhandled Exception";
                    return IResut;
                }
            }
            catch (Exception Ex)
            {
                IResut.pSuccess = false;
                IResut.pMessage = Ex.Message;
                return IResut;
            }
        }

        public Boolean IsWebSiteExist() { if (objWebInstaller == null) { objWebInstaller = new WebInstaller(); } return objWebInstaller.checkWebSiteExist(); }

        public bool UninstallBWPortal(string strDestinationPath)
        {
            InstallerResult IResut = new InstallerResult();

            try
            {
                if (strDestinationPath.LastIndexOf('\\') == strDestinationPath.Length - 1)
                {
                    strDestinationPath = strDestinationPath.Remove(strDestinationPath.Length - 1);
                }
                string msiPath = System.IO.Path.GetTempPath() + "\\BWServiceSetup.msi";
                System.IO.File.Copy(strDestinationPath + "\\BWPortal\\BWServiceSetup.msi", msiPath, true);
                if (System.IO.File.Exists(msiPath) == false)
                    return true;
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = "msiexec"; ///"msiexec /x """ + strTargetFolder_Path + "\setup.msi"" /quiet"
                p.StartInfo.Arguments = " /x " + "\"" + msiPath + "\"" + " /qb";
                //p.StartInfo.Arguments="msiexec.exe /i" "E:\My Steup Files\Installer.msi" /qn'
                p.StartInfo.UseShellExecute = true;
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.WaitForExit();
                if (p.ExitCode != 0)
                    return false;
                return true;
            }
            catch (Exception Ex)
            {
                IResut.pSuccess = false;
                IResut.pMessage = Ex.Message;
                return false;
            }
        }

        Boolean RunMSI(string msiPath, bool throwerror, bool IsReinsstall)
        {
            try
            {

                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.FileName = "msiexec";
                p.StartInfo.Arguments = " /i " + '"' + msiPath + '"' + " TARGETDIR=" + '"' + PathParam + '"' + " /qb";
                //p.StartInfo.Arguments="msiexec.exe /i" "E:\My Steup Files\Installer.msi" /qn'
                p.StartInfo.UseShellExecute = true;
                p.StartInfo.RedirectStandardOutput = false;
                p.Start();
                p.WaitForExit();
                if (p.ExitCode != 0 && throwerror)
                {
                    return false;
                    throw new Exception("Installation failed:Error in installing msi file");
                }{ return true; }
            }
            catch (Exception Ex)
            {
                throw (Ex);
            }
        }

        private void GetIisVersion()
        {
            using (Microsoft.Win32.RegistryKey componentsKey =
            Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\InetStp", false))
            {
                if (componentsKey != null)
                {
                    int majorVersion = (int)componentsKey.GetValue("MajorVersion", -1);
                    int minorVersion = (int)componentsKey.GetValue("MinorVersion", -1);
                    if (majorVersion != -1 && minorVersion != -1)
                    {
                        IISVersion = new Version(majorVersion, minorVersion);
                        return;
                    }
                }
                IISVersion = new Version(0, 0);
            }
        }
        private void CopyTempFiles()
        {
            try
            {
                System.IO.FileStream ServiceSetup = System.IO.File.Create(strTempFolder + "\\BWServiceSetup.msi");
                ServiceSetup.Write(Properties.Resources.BWServiceSetup, 0, Properties.Resources.BWServiceSetup.Length);
                ServiceSetup.Close();

            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }
        private void GetRegistryValue()
        {
            try
            {
                string RegistryPath = "";
                if (Environment.Is64BitOperatingSystem)
                {
                    RegistryPath = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\TWBS\BWPortal\";
                }
                else
                {
                    RegistryPath = @"HKEY_LOCAL_MACHINE\SOFTWARE\TWBS\BWPortal\";
                } 
                if (Registry.GetValue(RegistryPath + "BWService", "Version", "") != null)
                {
                    IsServiceInstalled = true;
                    SkipPortChecking = true;
                  
                }
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        private Boolean InstallerAction()
        {
            try
            {

                if (IsServiceInstalled)
                {
                    TWBS.UnInstaller.UnInstaller objUnInstall = new TWBS.UnInstaller.UnInstaller();
                    objUnInstall.UnInstallService(0);
                    
                    objWebInstaller.InitiliseIISManager();
                }
               
                if (objWebInstaller.IsPortUsed(objWebInstaller.IsEnableSSL, objWebInstaller.Port, false) == false)
                { 
                    objWebInstaller.Installer();
                    
                    Boolean installationCompleted = false;

                    if (objWebInstaller.IsService)
                    {

                        // RunMSI("E:\\Visual Studio 2010 Projects\\2.TEST Projects\\Rejish\\WebSetup1\\ServiceSetup\\Debug\\ServiceSetup.msi");
                        if (IsServiceInstalled)
                        {
                            installationCompleted = RunMSI(strTempFolder + "\\BWServiceSetup.msi", true, true);
                        }
                        else
                        {
                            installationCompleted = RunMSI(strTempFolder + "\\BWServiceSetup.msi", true, false);
                        }
                        //objWebInstaller.CopyClientAccessPolicyToIISRoot();
                    }
                    objWebInstaller.SetPermission();
                    // WriteLog("Calling SetConfigFileDetails", strTempFolder);
                    WriteLog("Website name:" + objWebInstaller.WebSiteName, strTempFolder);
                    return installationCompleted;

                }
                else
                {
                    throw (new Exception("Port :" + objWebInstaller.Port + " already used")); 

                }
            }
            catch (Exception Ex)
            {

                //  WriteLog(Ex.ToString(),strTempFolder);
                WriteLog(Ex, strTempFolder);

                throw (Ex);
            }
        }


        public void WriteLog(string Message, string Physicalpath)
        {
            try
            {


                TWBS.Common.ErrorLog ErLog = new TWBS.Common.ErrorLog();

                ErLog.EnableLog = true;
                ErLog.CreateLogFile(Physicalpath, "BWService");
                ErLog.WriteLogFile(Message);
            }
            catch (Exception ex)
            {

                throw (ex);
            }



        }
        private void WriteLog(Exception ex, string physicalPath)
        {
            if (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            StackTrace trace = new StackTrace(ex, true);
            string fileName = trace.GetFrame(0).GetFileName();
            int lineNo = trace.GetFrame(0).GetFileLineNumber();

            TWBS.Common.ErrorLog ErLog = new TWBS.Common.ErrorLog();

            ErLog.EnableLog = true;
            ErLog.CreateLogFile(physicalPath, "BWService");
            ErLog.WriteLogFile(fileName + " LineNo:" + lineNo.ToString() + "Message:" + ex.ToString());
        }



    }
}
