﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Security.AccessControl;
using Microsoft.Web.Administration;
using System.Xml;
using System.DirectoryServices;

namespace WebSetupInstaller
{
    internal class WebInstallerIIS6
    {
        Hashtable hstClientConfig = new Hashtable();
        Hashtable hstAdminConfig = new Hashtable();
        Hashtable hstconnectionConfig = new Hashtable();
        public string physicalpath = "";
        public bool IsClient, IsAdmin, IsService, IsEnableSSL;
        public string WebSiteName;
        public string strInetpubpath = "";
        public string Port = "8081";
        int intWebsiteID = 0;
       // private TWBS.General.DirectoryManager.IIS.Service service;


        public WebInstallerIIS6()
        {
            //service = new TWBS.General.DirectoryManager.IIS.Service("LocalHost");
        }
        public void CreateWebSite(string WebSiteName, string PortNo,bool EnableSSL)
         {
             try
             {
                 string strProtocol = "http";
                 if (EnableSSL)
                 {
                     strProtocol = "https";
                 }
                 CreateSite("IIS://localhost/BW", WebSiteName, strInetpubpath);
                 SetSingleProperty("IIS://localhost/BW/" + intWebsiteID.ToString(), "ServerBindings", ":" + PortNo + ":" + strProtocol);
             }
             catch (Exception Ex)
             {

                 throw (Ex);
             }
         }
        public void CreateApplication(string WebSiteID, string RootApplication, string PhysicalPath, string virtualDirectoryName)
         {
             try
             {
                 CreateVDir("IIS://Localhost/BW/" + WebSiteID + "/" + RootApplication, virtualDirectoryName, PhysicalPath);

             }
             catch (Exception Ex)
             {

                 throw (Ex);
             }
         }

         private void CreateSite(string metabasePath, string siteName, string physicalPath)
         {
             //  metabasePath is of the form "IIS://<servername>/<service>"
             //    for example "IIS://localhost/W3SVC" 
             //  siteID is of the form "<number>", for example "555"
             //  siteName is of the form "<name>", for example, "My New Site"
             //  physicalPath is of the form "<drive>:\<path>", for example, "C:\Inetpub\Wwwroot"

            

             try
             {
                 
                 DirectoryEntry service = new DirectoryEntry(metabasePath);
                 // Find unused ID value for new web site
                 int siteID = 1;
                 foreach (DirectoryEntry e in service.Children)
                 {
                     if (e.SchemaClassName == "IIsWebServer")
                     {
                         
                         int ID = Convert.ToInt32(e.Name);
                         if (ID >= siteID)
                         {
                             siteID = ID + 1;
                         }
                     }
                 }
                 intWebsiteID = siteID;
                 string className = service.SchemaClassName.ToString();
                 if (className.EndsWith("Service"))
                 {
                     DirectoryEntries sites = service.Children;
                     DirectoryEntry newSite = sites.Add(siteID.ToString(), (className.Replace("Service", "Server")));
                     newSite.Properties["ServerComment"][0] = siteName;
                     newSite.CommitChanges();

                     DirectoryEntry newRoot;
                     newRoot = newSite.Children.Add("Root", "IIsWebVirtualDir");
                     newRoot.Properties["Path"][0] = physicalPath;
                     newRoot.Properties["AccessScript"][0] = true;
                     newRoot.CommitChanges();
                 }
                 else
                    throw new Exception (" Failed. A site can only be created in a service node.");
             }
             catch (Exception ex)
             {
                 throw new Exception ("Failed in CreateSite with the following exception: "+ ex.Message);
             }
         }
         private void SetSingleProperty(string metabasePath, string propertyName, object newValue)
         {
             //  metabasePath is of the form "IIS://<servername>/<path>"
             //    for example "IIS://localhost/W3SVC/1" 
             //  propertyName is of the form "<propertyName>", for example "ServerBindings"
             //  value is of the form "<intStringOrBool>", for example, ":80:"

            
             try
             {
                 DirectoryEntry path = new DirectoryEntry(metabasePath);
                 PropertyValueCollection propValues = path.Properties[propertyName];
                 string oldType = propValues.Value.GetType().ToString();
                 string newType = newValue.GetType().ToString();
                 //Console.WriteLine(" Old value of {0} is {1} ({2})", propertyName, propValues.Value, oldType);
                 if (newType == oldType)
                 {
                     path.Properties[propertyName][0] = newValue;
                     path.CommitChanges();
                    // Console.WriteLine("Done");
                 }
                 else
                     throw new Exception(" Failed in SetSingleProperty; type of new value does not match property");
             }
             catch (Exception ex)
             {
                 if ("HRESULT 0x80005006" == ex.Message)
                     throw new Exception(string.Format(" Property {0} does not exist at {1}", propertyName, metabasePath));
                 else
                     throw new Exception(string.Format("Failed in SetSingleProperty with the following exception: \n{0}", ex.Message));
             }
         }
         private void CreateVDir(string metabasePath, string vDirName, string physicalPath)
         {
             //  metabasePath is of the form "IIS://<servername>/<service>/<siteID>/Root[/<vdir>]"
             //    for example "IIS://localhost/W3SVC/1/Root" 
             //  vDirName is of the form "<name>", for example, "MyNewVDir"
             //  physicalPath is of the form "<drive>:\<path>", for example, "C:\Inetpub\Wwwroot"
            // Console.WriteLine("\nCreating virtual directory {0}/{1}, mapping the Root application to {2}:",
                // metabasePath, vDirName, physicalPath);

             try
             {
                 DirectoryEntry site = new DirectoryEntry(metabasePath);
                 string className = site.SchemaClassName.ToString();
                 if ((className.EndsWith("Server")) || (className.EndsWith("VirtualDir")))
                 {
                     DirectoryEntries vdirs = site.Children;
                     DirectoryEntry newVDir = vdirs.Add(vDirName, (className.Replace("Service", "VirtualDir")));
                     newVDir.Properties["Path"][0] = physicalPath;
                     newVDir.Properties["AccessScript"][0] = true;
                     // These properties are necessary for an application to be created.
                     newVDir.Properties["AppFriendlyName"][0] = vDirName;
                     newVDir.Properties["AppIsolated"][0] = "1";
                     newVDir.Properties["AppRoot"][0] = "/LM" + metabasePath.Substring(metabasePath.IndexOf("/", ("IIS://".Length)));

                     newVDir.CommitChanges();

                     //Console.WriteLine(" Done.");
                 }
                 else
                    throw new Exception(" Failed. A virtual directory can only be created in a site or virtual directory node.");
             }
             catch (Exception ex)
             {
                throw new Exception(string.Format("Failed in CreateVDir with the following exception: \n{0}", ex.Message));
             }
         }


        public bool AddDirectorySecurity(string directoryPath, string userAccount, FileSystemRights rights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType controlType)
        {
            try
            {
                // Create a new DirectoryInfo object.
                DirectoryInfo dInfo = new DirectoryInfo(directoryPath);
                // Get a DirectorySecurity object that represents the 
                // current security settings.
                DirectorySecurity dSecurity = dInfo.GetAccessControl();
                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(new FileSystemAccessRule(userAccount, rights, inheritanceFlags, propagationFlags, controlType));
                // Set the new access settings.
                dInfo.SetAccessControl(dSecurity);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return true;
        }

        private void GetInetPubPath()
        {
            try
            {
                strInetpubpath = Environment.ExpandEnvironmentVariables(@"%SystemDrive%\Inetpub\wwwroot");
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }


    }
}
