﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.AccessControl;
using Microsoft.Web.Administration;
using System.Xml;
using System.Collections;
using System.Security.Principal;

namespace WebSetupInstaller
{
    internal class WebInstaller
    { 
        public string physicalpath = "";
        public byte[] SSLCertificateHash=null;
        public bool  IsService=true, IsEnableSSL, IsDefaultWebsite = true,IsUpgrade=false;
        public string WebSiteName = "Versago";//"BWPortal";
        public string DefaultWebSiteName = "Default Web Site";
        public string strInetpubpath = "";
        private string strUserServicePhysicalPath = "";
        public string Port = "80";
        public string appPool = "Versago";//"BWPortal";

        public string strNewPort = "";
      
        ServerManager iisManager;
        Version IISVersion;
        public  WebInstaller() { if (iisManager == null) { iisManager = new ServerManager(); } }
        public Boolean checkWebSiteExist() {
            if (iisManager == null) { iisManager = new ServerManager(); }
            Site site = iisManager.Sites[WebSiteName]; if (site == null) { return false; } else { return true; } }
        public void CreateApplicationPool()
        {
            try
            {
                ApplicationPool newAppPool;
                newAppPool = iisManager.ApplicationPools[appPool];
                if (newAppPool == null)
                {
                    newAppPool = iisManager.ApplicationPools.Add(appPool);
                    newAppPool.ProcessModel.IdentityType = ProcessModelIdentityType.NetworkService;
                    newAppPool.ManagedRuntimeVersion = "v4.0";
                    newAppPool.AutoStart = true;
                    newAppPool.Enable32BitAppOnWin64 = false;
                    newAppPool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    iisManager.CommitChanges();
                }

            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        } 
        public void CreateWebSite(string WebSiteName, string PortNo, bool EnableSSL,string webPath,byte[] SSLCertificateHash)
        {
            try
            {
                //bool PortUsed=IsPortUsed(EnableSSL, PortNo);
                //if (PortUsed == false)
                //{
                strNewPort = PortNo;
                Site site = iisManager.Sites[WebSiteName];
                if (site == null)
                {
                  //  System.Windows.Forms.MessageBox.Show("Start website creation");
                    
                    if (EnableSSL)
                    {
                        //byte[] toBytes = Encoding.ASCII.GetBytes(SSLCertificateHash);
                        if (SSLCertificateHash != null)
                        {
                            iisManager.Sites.Add(WebSiteName, "*:" + PortNo + ":", webPath, SSLCertificateHash); 
                        }
                        else {  iisManager.Sites.Add(WebSiteName,"https", "*:" + PortNo + ":", webPath);}
                    }else{
                          iisManager.Sites.Add(WebSiteName, "http", "*:" + PortNo + ":", webPath);//"C:\\inetpub\\wwwroot"
                    }
                  
                    iisManager.CommitChanges();
                    if (EnableSSL)
                    {
                        EnableSSLIIS(WebSiteName);
                    }
                    
                   // CreateApplication(WebSiteName);
                }
                //else
                //{
                //    throw new Exception("Website :" + WebSiteName + " already exist");
                //}
                //}
                //else
                //{
                //    throw new Exception("Port :" + PortNo + " already used");
                //}
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }
        public void CreateApplication(string WebSite, string ApplicationPath, string PhysicalPath, string virtualDirectoryPath)
        {
            try
            {
                Site site = iisManager.Sites[WebSite];
                if (site != null)
                {
                    Application app = site.Applications[ApplicationPath];
                    if (app == null)
                    {
                        app = site.Applications.CreateElement();
                        app.Path =  ApplicationPath;
                        app.ApplicationPoolName =appPool;
                         

                        VirtualDirectory vDir = app.VirtualDirectories.CreateElement();
                        vDir.Path = "/";
                        vDir.PhysicalPath = PhysicalPath;

                        app.VirtualDirectories.Add(vDir);
                        site.Applications.Add(app);
                        iisManager.CommitChanges();

                    }
                }
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

        public void Installer()
        {
            try
            {
                if (!checkWebSiteExist())
                {
                    // AddDirectorySecurity(physicalpath + "\\APP Portal", "Users", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);
                    if (WebSiteName != DefaultWebSiteName)
                    {
                        IsDefaultWebsite = false;
                    }
                    CreateApplicationPool();
                    // System.Windows.Forms.MessageBox.Show(WebSiteName +":" + DefaultWebSiteName);

                    if (IsDefaultWebsite == false)
                    {
                        // System.Windows.Forms.MessageBox.Show("Start IsDefaultWebsite == false");
                        CreateWebSite(WebSiteName, Port, IsEnableSSL, physicalpath, SSLCertificateHash);
                    } 
                }
                if (IsService)
                {
                    CreateApplication(WebSiteName, "/BWService", physicalpath + "\\BWService", "/BWService");
                }
            }
            catch (Exception Ex)
            {
                WriteLog(Ex.ToString(), physicalpath);
                throw (Ex);
            }
        }

        public bool AddDirectorySecurity(string directoryPath, string userAccount, FileSystemRights rights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType controlType)
        {
            try
            {     
                DirectorySecurity sec = Directory.GetAccessControl(directoryPath);
                WindowsIdentity id = WindowsIdentity.GetCurrent();
                // Using this instead of the "Everyone" string means we work on non-English systems.
                SecurityIdentifier User = new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null);
                sec.AddAccessRule(new FileSystemAccessRule(User, FileSystemRights.FullControl | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                Directory.SetAccessControl(directoryPath, sec);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return true;
        }

        public void SetPermission()
        {
            try
            {
               AddDirectorySecurity(physicalpath , "Users", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, AccessControlType.Allow);
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

         
       
        public void GetIisVersion()
        {

            using (Microsoft.Win32.RegistryKey componentsKey =
            Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\InetStp", false))
            {
                if (componentsKey != null)
                {
                    int majorVersion = (int)componentsKey.GetValue("MajorVersion", -1);
                    int minorVersion = (int)componentsKey.GetValue("MinorVersion", -1);
                    if (majorVersion != -1 && minorVersion != -1)
                    {
                        IISVersion = new Version(majorVersion, minorVersion);
                    }
                }
                IISVersion = new Version(0, 0);
            }
        }

        private void GetInetPubPath()
        {
            try
            {
                strInetpubpath = Environment.ExpandEnvironmentVariables(@"%SystemDrive%\Inetpub\wwwroot");
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

        private void EnableSSLIIS(string WebSiteName)
        {
            try
            {
                Configuration config = iisManager.GetApplicationHostConfiguration();
                ConfigurationSection accessSection = config.GetSection("system.webServer/security/access", WebSiteName);
                
                accessSection["sslFlags"] = @"Ssl";
                iisManager.CommitChanges();
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

        public bool IsPortUsed(bool EnableSSL, string Port, bool GetDefaultWebSite)
        {
            try
            {
                GetInetPubPath();
                if (iisManager == null)
                {
                    iisManager = new ServerManager();
                }
               

                if (GetDefaultWebSite == false && IsUpgrade==false)
                {
                    if (WebSiteName != DefaultWebSiteName)
                    {
                        IsDefaultWebsite = false;
                    }
                    if (IsDefaultWebsite == false)
                    {
                        string strProtocol = "http";
                        if (EnableSSL)
                        {
                            strProtocol = "https";
                        }
                        foreach (Site wbSite in iisManager.Sites)
                        {
                            foreach (Microsoft.Web.Administration.ConfigurationElement binding in wbSite.GetCollection("bindings"))
                            {
                                string protocol = (string)binding["protocol"];
                                string bindingInfo = (string)binding["bindingInformation"];

                                if (protocol.StartsWith(strProtocol, StringComparison.OrdinalIgnoreCase))
                                {
                                    string[] parts = bindingInfo.Split(':');
                                    if (parts.Length == 3)
                                    {
                                        //Get the port in use HERE !!!
                                        string port = parts[1];
                                        if (port == Port)
                                        {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (Site wbSite in iisManager.Sites)
                    {
                         //string strProtocol="http";
                         //if (IsEnableSSL == true)
                         //    strProtocol = "https";
                         //Boolean status = true;
                       

                        foreach (Microsoft.Web.Administration.ConfigurationElement binding in wbSite.GetCollection("bindings"))
                        {
                            string protocol = (string)binding["protocol"];
                            string bindingInfo = (string)binding["bindingInformation"];

                            //if (protocol == strProtocol && wbSite.Name==WebSiteName)
                            //    status= false;

                            string[] parts = bindingInfo.Split(':');
                            if (parts.Length == 3)
                            {
                                //Get the port in use HERE !!!
                                string port = parts[1];
                                if (port == Port)
                                {
                                    DefaultWebSiteName = wbSite.Name;
                                    break;
                                }
                            }

                        }

                        //if (status == true && wbSite.Name == WebSiteName )
                        //{
                        //    wbSite.Bindings.Add("", strProtocol);
                        //}

                        

                    }
                }
                return false;
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        } 

        public void WriteLog(string Message ,string Physicalpath)
        {
            try
            {
                TWBS.Common.ErrorLog ErLog = new TWBS.Common.ErrorLog();
               
                ErLog.EnableLog = true;
                ErLog.CreateLogFile(Physicalpath, "BWServiceSetup");
                ErLog.WriteLogFile(Message);
            }
            catch (Exception ex)
            {

                throw (ex);
            }
            


        }
        public void InitiliseIISManager()
        {

            iisManager = new ServerManager();
        
        }

        public int GetPortNo()
        {
            try
            {
                int portNo = Convert.ToInt32(Port);
               
                if (iisManager == null)
                {
                    iisManager = new ServerManager();
                }


               
              
                    foreach (Site wbSite in iisManager.Sites)
                    {
                        if (wbSite.Name == WebSiteName)
                        {
                            foreach (var SiteBinding in wbSite.Bindings)
                            {
                                portNo = SiteBinding.EndPoint.Port;
                                
                                return portNo;
                            }
                        }

                                               

                    }

                    return portNo;
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }
        public Boolean GetProtocol()
        {
            try
            {
                Boolean Protocol = false;

                if (iisManager == null)
                {
                    iisManager = new ServerManager();
                }




                foreach (Site wbSite in iisManager.Sites)
                {
                    if (wbSite.Name == "APP Portal")
                    {
                        foreach (var SiteBinding in wbSite.Bindings)
                        {
                            if (SiteBinding.Protocol == "https")
                                Protocol = true;
                            return Protocol;
                        }
                    }



                }

                return Protocol;
            }
            catch (Exception Ex)
            {

                throw (Ex);
            }
        }

       

        private void WriteLog(Exception ex, string physicalPath)
        {
            if (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
            string fileName = trace.GetFrame(0).GetFileName();
            int lineNo = trace.GetFrame(0).GetFileLineNumber();

            TWBS.Common.ErrorLog ErLog = new TWBS.Common.ErrorLog();

            ErLog.EnableLog = true;
            ErLog.CreateLogFile(physicalPath, "BWServiceSetup");
            ErLog.WriteLogFile(fileName + " LineNo:" + lineNo.ToString() + "Message:" + ex.ToString());
        }
    }
}
