﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System.Web.Http.Cors;


namespace BW.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class WorkFlowController : ApiController
    {
        //[HttpGet]
        //public string InvokeWorkFlow(int pTaskID, int pStartAfter,string  pArguments)
        //{
        //    Models.workflowRequest wfRequest = new Models.workflowRequest();
        //    wfRequest.pTaskID = pTaskID;
        //    wfRequest.pStartAfter = pStartAfter;
        //    wfRequest.pArguments = pArguments;
        //    return StartWorkFlow(wfRequest);
        //}
        [HttpGet]
       // [ActionName("getArgs")]
        public string getArguments(int pTaskID)
        {
            if (pTaskID > 0)
            {
                BW.BusinessLayer.blWorkFlow ObjWKF = new BusinessLayer.blWorkFlow();
                string jArgs= ObjWKF.getArguments(pTaskID).Replace("/","");
                return jArgs;
            }
            return "";
        }
        [HttpPost]
        [ActionName("InvokeWorkFlow")]
        public string InvokeWorkFlow([FromBody] Models.workflowRequest wfRequest)
        {

            return StartWorkFlow(wfRequest);

        }

        private string StartWorkFlow(Models.workflowRequest wfRequest)
        {
            if (wfRequest.pTaskID > 0)
            {
                BW.BusinessLayer.blWorkFlow ObjWKF = new BusinessLayer.blWorkFlow();
                return ObjWKF.InvokeWorkFlow(wfRequest);
            }
            else return Models.eResponse.INVALIDTASK.ToString();
        }
    }
}
