﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BW.Models
{
    public class workflowRequest
    { 
       public int pTaskID { get; set; }
        public int pStartAfter { get; set; }
        public string pArguments { get; set; }
    }
}