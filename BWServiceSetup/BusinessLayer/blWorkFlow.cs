﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;

namespace BW.BusinessLayer
{
    public class blWorkFlow
    {
        string settingsPath = string.Format("{0}\\{1}", Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData), "Bizweaver");
        public string InvokeWorkFlow(Models.workflowRequest objWrkFlow)
        {
            try
            {
            
            if (objWrkFlow.pTaskID>-1)
            { 
                    BizweaverCommanderInterpreter.Interpreter interpreter = new BizweaverCommanderInterpreter.Interpreter(settingsPath);
                    string strArgument = createParameters(objWrkFlow.pArguments);
                    string outMsg = string.Empty; 
                    Object objRes=interpreter.InitiateTaskExecution(objWrkFlow.pTaskID, strArgument,out outMsg);
                    if (outMsg == string.Empty)
                        return Models.eResponse.SUCCESS.ToString();
                    else
                        return Models.eResponse.SUCCESS.ToString();
                }
            else { return Models.eResponse.ERROR.ToString(); }
            }
            catch (Exception)
            {
                return Models.eResponse.ERROR.ToString();
            }
        }
        private string createParameters(string pParams)
        {
            var dParams = JsonConvert.DeserializeObject<Dictionary<string, string>>(pParams);

            string strParam = "<ArgumentTable>";
            foreach (KeyValuePair<string, string> param in dParams)
            {
                strParam+="<"+param.Key+">"+ param.Value +"</" + param.Key + ">";
            }
            return strParam+ "</ArgumentTable>";
        }
        private string getJSONParameters(string pParams)
        {
            try
            {
                string pArguments = "{";
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(pParams);

                XmlNode xmlParams = doc.SelectSingleNode("ArgumentTable");
                if (xmlParams.ChildNodes.Count >= 1)
                {
                    foreach (XmlNode echParam in xmlParams.ChildNodes)
                    {
                        pArguments += '"' + echParam.Name + '"' + ':' + '"'+ echParam.InnerText + '"';
                        if (xmlParams.LastChild.Name != echParam.Name)
                        {
                            pArguments = pArguments + ',';
                        }
                    }
                }
                pArguments = pArguments + '}';
                return pArguments;
            }
            catch (Exception)
            {

                return "";
            }
           
        }
        public string getArguments(int TaskID)
        {
            BizweaverCommanderInterpreter.Interpreter interpreter = new BizweaverCommanderInterpreter.Interpreter(settingsPath);
            string bwArguments =interpreter.InitiateArgumentsListRequest(TaskID);
            return getJSONParameters(bwArguments);
        }
      
    }
}
